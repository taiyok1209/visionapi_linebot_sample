const path = require('path');
const os = require('os');
const fs = require('fs');

/**
 * 画像の投稿を処理するクラス
 *
 * @class ImageProcessor
 */
class ImageProcessor {

  /**
   *Creates an instance of ImageProcessor.
   * @param {*} admin
   * @param {*} client
   * @memberof ImageProcessor
   */
  constructor(admin, lineClient, visionClient) {
    this.firebaseAdmin = admin;
    this.lineClient = lineClient;
    this.visionClient = visionClient;

    this.possibilities = {
      'UNKNOWN': 0,
      'VERY_UNLIKELY': 1,
      'UNLIKELY': 2,
      'POSSIBLE': 3,
      'LIKELY': 4,
      'VERY_LIKELY': 5
    };
    this.emotions = {
      'joyLikelihood': '喜んで',
      'angerLikelihood': '怒って',
      'sorrowLikelihood': '悲しんで',
      'surpriseLikelihood': '驚いて'
    }
  }

  /**
   * 画像を保存し、投稿者の情報を保存してメッセージを返す
   *
   * @param {*} event
   * @returns
   * @memberof ImageProcessor
   */
  async saveImageContentAndFaceEmotionDetection(event) {
    const imagePath = await this.getLineImageContent(event.message.id);
    const [file, metadata] = await this.uploadImageContent(imagePath);
    await this.saveMessageInfo(event, file.name);
    const faceEmotions = await this.faceEmotionDetection(imagePath);
    const message = this.createMessage(faceEmotions);
    // console.log(labelDescriptions.join(os.EOL));
    return this.lineClient.replyMessage(event.replyToken, {
      type: 'text',
      text: message
    });
  }

  /**
   * Lineに投稿されたコンテンツを取得する
   *
   * @param {*} messageId
   * @returns
   * @memberof ImageProcessor
   */
  async getLineImageContent(messageId) {
    const ext = '.jpg';
    const tmpPath = path.join(os.tmpdir(), messageId + ext);
    const stream = await this.lineClient.getMessageContent(messageId);
    return new Promise((resolve, reject) => {
      const writable = fs.createWriteStream(tmpPath);
      stream.pipe(writable);
      stream.on('end', () => {
        resolve(tmpPath);
      });
      stream.on('error', reject);
    });
  }

  /**
   * Storageにファイルを保存する。
   *
   * @param {*} imagePath
   * @returns
   * @memberof ImageProcessor
   */
  async uploadImageContent(imagePath) {
    return this.firebaseAdmin.storage().bucket().upload(imagePath);
  }

  /**
   * 画像の顔感情を検出する。
   *
   * @param {*} imagePath
   * @returns
   * @memberof ImageProcessor
   */
  async faceEmotionDetection(imagePath) {
    const [result] = await this.visionClient.faceDetection(imagePath);
    const faces = result.faceAnnotations;
    console.log('Faces:');
    const facesResults = faces.map((face, i) => {
      let result = {};
      for (const [emotion, label] of Object.entries(this.emotions)) {
        const possibility = face[emotion];
        result[label] = this.possibilities[possibility];
      }
      return result;
    });
    console.log(facesResults);
    return facesResults;
  }

  /**
   * メッセージの情報（投稿者、時間、画像）を保存する。
   *
   * @param {*} info
   * @param {*} image
   * @returns
   * @memberof ImageProcessor
   */
  saveMessageInfo(info, image) {
    var savedInfo = {
      source: info.source,
      timestamp: info.timestamp,
      image: image,
    };
    return this.firebaseAdmin.database().ref('/messages').push(savedInfo);
  }

  /**
   * 感情検出の結果メッセージを作成する。
   *
   * @param {*} faceEmotions
   * @returns
   * @memberof ImageProcessor
   */
  createMessage(faceEmotions) {
    if (faceEmotions.length == 0) {
      return '顔を検出できませんでした。';
    }

    const emotionMessages = faceEmotions.map(emotion => {
      const maxEmotion = Object.keys(emotion).reduce((a, b) => emotion[a] > emotion[b] ? a : b);
      if (emotion[maxEmotion] < 3) {
        return '感情を検出できませんでした。';
      } else if (emotion[maxEmotion] == 5) {
        return 'とても' + maxEmotion + 'そうですね！';
      } else {
        return maxEmotion + 'そうですね！';
      }
    });
    const returnMessage = emotionMessages.reduce((returnMessage, emotionMessage, index) => {
      returnMessage += (index + 1) + '人目：' + os.EOL + '　' + emotionMessage + os.EOL;
      return returnMessage;
    }, '');
    return returnMessage;
  }
}

module.exports = ImageProcessor;