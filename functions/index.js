const functions = require('firebase-functions');
const line = require('@line/bot-sdk');

// Firebase
const admin = require('firebase-admin');
admin.initializeApp();

// create LINE SDK config from env variables
const config = {
  channelAccessToken: functions.config().line.channel_access_token,
  channelSecret: functions.config().line.channel_secret,
};

// create LINE SDK client
const lineClient = new line.Client(config);

// create Vision API client
const vision = require('@google-cloud/vision');
const visionClient = new vision.ImageAnnotatorClient();

// 処理クラス
const TextMessageProcessor = require('./text');
const textMessageProcessor = new TextMessageProcessor(admin, lineClient);
const ImageProcessor = require('./image');
const imageProcessor = new ImageProcessor(admin, lineClient, visionClient);

// LINE の応答を生成
exports.line = functions
  .region('asia-northeast1')
  .https.onRequest(async (request, response) => {
    try {
      const result = await Promise.all(request.body.events.map(handleEvent));
      response.json(result);
    } catch (err) {
      console.error(err);
      response.status(500).end();
    }
  });

/**
 * LINE からのメッセージを処理する *
 * @param {*} event
 * @returns
 */
function handleEvent(event) {
  console.log(JSON.stringify(event));
  if (event.type !== 'message') {
    // ignore non-text-message event
    return Promise.resolve(null);
  }

  const messageType = event.message.type;
  if (messageType === 'text') {
    return textMessageProcessor.saveAndReplyTextMessage(event); // テキスト処理クラスで処理
  } else if (messageType === 'image') {
    return imageProcessor.saveImageContentAndFaceEmotionDetection(event); // 画像処理クラスで処理
  } else {
    return Promise.resolve(null);
  }
}